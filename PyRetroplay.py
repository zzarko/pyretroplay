#!/usr/bin/python
# -*- coding: UTF-8 -*-
# encoding=utf8  

#    PyRetroplay (c) 2017 Žarko Živanov

#    PyRetroplay is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import json, urllib, urllib2, os, subprocess, re, time, shutil, codecs, progressbar, logging, sys
from ftplib import FTP
from sys import stdout

VERSION = "1.2.6"

#########################################################################
#                       PyRetroplay SETTINGS
#                - change them to suit your needs... -
#########################################################################

# PC game/demo directory paths relative to script
#    Demos  - path to Demos directory
#    Games  - path to Games
#    AGA    - path to AGA Games directory
#    Local  - local copy of retroplay's files, with directory structure as in
#             ftp://grandis.nu/Commodore_Amiga/Retroplay/
#             If an archive exists in local copy, it won't be downloaded when updating
Demos   = u"Demos_WHDLoad"
Games   = u"Games_WHDLoad"
AGA     = u"Games_WHDLoad_AGA"
Local   = u"Retroplay"

# Archive selection options
#   ProcessAGAGames     - process AGA games
#   ProcessECSGames     - process ECS games
#   ProcessDemos        - process demos
#   ProcessJSTGames     - process archives with JST and JOTD suffixes (HD_Loaders_-_Games and JST_-_Games)
#   ProcessBetaGames    - process beta archives (Beta_&_Unreleased)
#   ProcessMagazines    - process magazines
#   PreferredLanguages  - list of space separated two-letter preferred languages (see Languages variable below)
#   CheckFTP            - check new files on ftp://grandis.nu/Commodore_Amiga/Retroplay/
#   UpdateFromFTP       - download new files from ftp://grandis.nu/Commodore_Amiga/Retroplay/
#   GenerateAmigaScript - generate AmigaDOS script for updating on real Amiga
#   DeleteNonFTPGames   - automatically delete games that were removed from Retroplay collection (False is recommended)
ProcessAGAGames     = True
ProcessECSGames     = True
ProcessDemos        = True
ProcessJSTGames     = False # not yet implemented
ProcessBetaGames    = False # not yet implemented
ProcessMagazines    = False # not yet implemented
PreferredLanguages  = ""    # not yet implemented
CheckFTP            = True
UpdateFromFTP       = True
GenerateAmigaScript = True
DeleteNonFTPGames   = False

#########################################################################
#                       PyRetroplay variables
#########################################################################

# language suffixes
Languages = "En De Fr It Se Pl Cz Cs Es Dk Fi Gr"
LangList = Languages.split()

# archive information
ArchDataFile    = "archivedata.json"    # archives data file
Archives        = {'bydir' : {}, 'byarc' : {}}  # dictionary for archives data

# Retroplay FTP directories
RetroplayPath   = "Commodore_Amiga/Retroplay"
RetroJOTDDir    = "Commodore_Amiga_-_HD_Loaders_-_Games"
RetroJSTDir     = "Commodore_Amiga_-_JST_-_Games"
RetroDemosDir   = "Commodore_Amiga_-_WHDLoad_-_Demos"
RetroGamesDir   = "Commodore_Amiga_-_WHDLoad_-_Games"
RetroBetaDir    = "Commodore_Amiga_-_WHDLoad_-_Games_(Beta_&_Unreleased)"
RetroMagDir     = "Commodore_Amiga_-_WHDLoad_-_Magazines"

# path where script resides
ownpath = os.path.realpath(__file__)
ownpath = os.path.dirname(ownpath)

# set to true for debugging
Debug = False
#Debug = True

#########################################################################
#                               Logging
#########################################################################

class InfoFormatter(logging.Formatter):
    FORMATS = {logging.INFO : "%(message)s",
               'DEFAULT' : "%(levelname)s: %(message)s"}
    def format(self, record):
        self._fmt = self.FORMATS.get(record.levelno, self.FORMATS['DEFAULT'])
        return logging.Formatter.format(self, record)
# create logger
logger = logging.getLogger('PyRetroplay')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler('PyRetroplay.log',encoding = 'utf-8')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
# create formatter and add it to the handlers
formatter = InfoFormatter()
ch.setFormatter(formatter)
fh.setFormatter(formatter)
# add the handlers to logger
logger.addHandler(ch)
logger.addHandler(fh)

#########################################################################
#                PyRetroplay AmigaDOS script generation
#########################################################################

AmigaDosStart='''; Script to update Retroplay's collection on real Amiga
; You can execute it with:
;       execute update##date##
; Generated with PyRetroplay by Zarko Zivanov

; The script requires LHA and LZH archivers from:
; http://aminet.net/package/util/arc/lha_68k
; http://aminet.net/package/util/arc/lzx121r1

ECHO ""
ECHO "Retroplay collection update ##date##"
ECHO ""

ECHO "Checking games/demos directories ..."
IF NOT EXISTS "##games##/"
    ECHO "##games##/ not found!"
    QUIT 5
ENDIF
IF NOT EXISTS "##aga##/"
    ECHO "##aga##/ not found!"
    QUIT 5
ENDIF
IF NOT EXISTS "##demos##/"
    ECHO "##demos##/ not found!"
    QUIT 5
ENDIF
ECHO ""

'''

AmigaDosUpdateStart='ECHO "This script will update the following games/demos:"\n'
AmigaDosUpdateList='ECHO "##path##/"\n'

AmigaDosContinue='''
ECHO ""
ASK "Do you want to continue?"
IF NOT WARN
    ECHO "Exiting..."
    ECHO ""
    QUIT 5
ENDIF

SET CURRDIR `CD`
ECHO ""

'''

AmigaDosUpdateLha='''IF EXISTS "##path##/"
    ECHO "Deleting ##path##/ ..."
    DELETE "##path##/" ALL QUIET
ENDIF
IF EXISTS "##path##.info"
    DELETE "##path##.info" QUIET
ENDIF
ECHO "Unpacking to ##path##/ ..."
CD "##paths##/"
LHA -I -q x "//##arc##"
CD "$CURRDIR"

'''

AmigaDosUpdateLzx='''IF EXISTS "##path##/"
    ECHO "Deleting ##path##/ ..."
    DELETE "##path##/" ALL QUIET
ENDIF
IF EXISTS "##path##.info"
    DELETE "##path##.info" QUIET
ENDIF
ECHO "Unpacking to ##path##/ ..."
CD "##paths##/"
LZX -q x "//##arc##"
CD "$CURRDIR"

'''

AmigaDosDeleteStart='ECHO "This script will delete the following games/demos:"\n'
AmigaDosDeleteList='ECHO "##path##/"\n'

AmigaDosDeleteCmd='''IF EXISTS "##path##/"
    ECHO "Deleting ##path##/ ..."
    DELETE "##path##/" ALL QUIET
ENDIF
IF EXISTS "##path##.info"
    DELETE "##path##.info" QUIET
ENDIF

'''

AmigaDosEnd='''ECHO ""
ECHO "Retroplay collection updated to ##date##"
ECHO ""
'''


#########################################################################
#                       PyRetroplay main code
#########################################################################

#execute bash command
def bash(command, workdir=None):
    """Helper function to execute bash commands"""
    #command = shlex.split(command.encode("utf-8"))
    if workdir == None:
        workdir = ownpath
    try:
        process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                                   shell=True, cwd=workdir)
        output, unused_err = process.communicate()
        code = process.poll()
    except:
        code = 127
        output = ""
    return code,output

# get directory name from within archive
def getArchiveDirLHASA(path):
    code, output = bash('lhasa -l "%s"' % path)
    arcdir = ""
    arcname = os.path.basename(path)
    #print("0:\n", arcname)
    if code == 0:
        arcdir = output.split("\n")
        # find position of last space in second line (line with dashes)
        # that is the starting column for archive contents
        pos = arcdir[1].rfind(" ") + 1
        #print("1:\n", arcdir)
        arcdir = filter(lambda x: "/" in x, arcdir)
        #print("2:\n", arcdir)
        arcdir = (x[pos:] for x in arcdir)
        #print("A:\n", arcdir)
        arcdir = filter(lambda x: not arcname.encode("utf-8") in x,arcdir)
        #print("3:\n", arcdir)
        arcdir = arcdir[0].split("/")[0]
    if arcdir == "":
        stdout.write("\n")
        stdout.flush()
        logger.error(u"Something went wrong while listing the archive\n%s" % path)
        logger.debug(u"Reported error:\n%s" % output)
        #exit(1)
    return arcdir

# get directory name from within archive
def getArchiveDirUNAR(path):
    code, output = bash('lsar "%s"' % path)
    arcdir = ""
    arcname = os.path.basename(path)
    if code == 0:
        arcdir = output.split("\n")
        arcdir = filter(lambda x: "/" in x, arcdir)
        arcdir = filter(lambda x: not arcname.encode("utf-8") in x,arcdir)
        arcdir = arcdir[0].split("/")[0]
    if (arcdir == "") or (code != 0):
        logger.debug(u"lsar: something went wrong while listing the archive\n%s" % path)
        logger.debug(u"Reported error:\n%s" % output)
        logger.debug(u"Trying the same archive with lhasa...")
        arcdir = getArchiveDirLHASA(path)
        #exit(1)
    return arcdir


# get directory name from within archive
def getArchiveDir(path):
    code, output = bash('7z -ssc -sccutf-8 l "%s"' % path)
    arcdir = ""
    arcname = os.path.basename(path)
    #print("0:\n", arcname)
    if code == 0:
        arcdir = output.split("\n")
        posline = filter(lambda x: "---------" in x, arcdir)
        # find position of last space in second line (line with dashes)
        # that is the starting column for archive contents
        pos = posline[0].rfind(" ") + 1
        #print("P %d:" % pos,posline)
        arcdir = filter(lambda x: not arcname.encode("utf-8") in x,arcdir)
        #print("1:\n", arcdir)
        arcdir = filter(lambda x: "/" in x, arcdir)
        #print("2:\n", arcdir)
        arcdir = list(x[pos:] for x in arcdir)
        #print("3:\n", arcdir[0])
        arcdir = arcdir[0].split("/")[0]
    if (arcdir == "") or (code != 0):
        output2 = output.split("\n")
        index = [i for i, s in enumerate(output2) if 'Listing archive:' in s]
        if len(index) > 0:
            output2 = output2[index[0]:]
            output2 = [s for s in output2 if s != ""]
            output = "\n".join(output2)
        logger.debug(u"7zip: something went wrong while listing the archive\n%s" % path)
        logger.debug(u"Reported error:\n%s" % output)
        logger.debug(u"Trying the same archive with unarchiver...")
        arcdir = getArchiveDirUNAR(path)
        #exit(1)
    return arcdir

#arcdir=getArchiveDir("./TESTING/RedLightning_v1.0_NTSC_1781.lha")
#print("#%s#" % arcdir)
#exit(0)

# check FTP for current file list
def checkFTPFiles(pathlist, FTPList, ftp):
    for path in pathlist:
        out = "Checking " + path + " ..."
        stdout.write("\r%s\r%s" % (" "*(len(out)+15), out))
        stdout.flush()
        time.sleep(1)
        ftp.cwd("/")
        ftp.cwd(RetroplayPath + "/" + path + "/")
        ftpdir = []
        ftp.retrlines("LIST",ftpdir.append)
        files = [path + "/" + line.split()[8] for line in ftpdir if line[0]=="-"]
        dirs = [path + "/" + line.split()[8] for line in ftpdir if line[0]=="d"]
        #print("FILES:\n",files)
        #print("DIRS:\n",dirs)
        if files != []:
            FTPList.extend(files)
        if dirs != []:
            checkFTPFiles(dirs, FTPList, ftp)

# check if there are new archives, or new versions
def checkLocalArchives(path, category, ArchList):
    rversion = re.compile(r'_v([0-9][.0-9a-z]*)_?', flags=re.IGNORECASE)
    raga = re.compile(r'_(AGA|CD32)', flags=re.IGNORECASE)
    rdemo = re.compile(r'(Demo|Demos|Preview|Prv|Prerelease)$')
    rcode = re.compile(r'_([0-9]{4}(?:&[0-9]{4})*)$')
    rntsc = re.compile(r"_NTSC", flags=re.IGNORECASE)
    rlang = re.compile(r"_("+r"|".join(LangList)+r")+", flags=re.IGNORECASE)
    for root, dirs, files in os.walk(unicode(path)):
        dirs.sort()
        files.sort()
        out = "Scanning " + root + " ..."
        stdout.write("\r%s\r%s" % (" "*(len(out)+15), out))
        stdout.flush()
        for name in files:
            arcpath = os.path.join(root, name)  #local path to the archive
            arcname, extension = os.path.splitext(name)
            #print("arcpath:",arcpath)
            #print("arcname:",arcname)
            if not arcpath in Archives['byarc']:
                try:
                    version = ''
                    language = 'En'
                    disks = ''
                    demo = False
                    pal = True
                    memtype = ''
                    memsize = ''
                    amiga = 'ECS'
                    code = ''
                    arcdir = getArchiveDir(arcpath) #name of the directory inside archive
                    letter = os.path.basename(root)
                    namesplit = rcode.split(arcname)    #remove 4-digit code(s) from end
                    if len(namesplit) > 1:
                        code = namesplit[1]
                        namesplit = rversion.split(namesplit[0])
                    else:
                        namesplit = rversion.split(arcname)
                    version = namesplit[1]  #extract installer version
                    del namesplit[1]
                    if namesplit[-1] == "": del namesplit[-1]
                    archive = "_".join(namesplit)
                    if code != "": archive = archive + "_" + code
                    restname = "_" + "_".join(namesplit[1:])  #rest of archive name
                    if raga.search(restname):
                        amiga = raga.search(restname).group(1)
                    if rntsc.search(restname):
                        pal = False
                    if rlang.search(restname):
                        language = rlang.search(restname).group(1)
                    if category == "demos":
                        unpackpath = os.path.join(Demos,letter,arcdir)
                        unpackpath = unpackpath.replace("/0/","/0-9/")
                    elif category == "games":
                        if amiga == "ECS":
                            path = Games
                        else:
                            path = AGA
                        unpackpath = os.path.join(path,letter,arcdir)
                        unpackpath = unpackpath.replace("/0/","/0-9/")
                        if rdemo.search(namesplit[0]) or rdemo.search(restname):
                            demo = True
                    Archives['byarc'][arcpath] = unpackpath
                    if not unpackpath in Archives['bydir']:
                        Archives['bydir'][unpackpath] = []
                    newdata = {}
                    newdata['archivefull'] = arcpath
                    newdata['archive'] = archive
                    newdata['version'] = version
                    newdata['language'] = language
                    newdata['disks'] = disks
                    newdata['demo'] = demo
                    newdata['pal'] = pal
                    newdata['memtype'] = memtype
                    newdata['memsize'] = memsize
                    newdata['amiga'] = amiga
                    Archives['bydir'][unpackpath].append(newdata)
                except Exception, e:
                    logger.error(u'Problem detected for archive:\n%s' % arcpath)
                    logger.debug('Exception caught: %s' % str(e))

# check for required programs
missingprg = False
code, output = bash("which 7z")
if code != 0:
    logger.error("You need 7zip for this script to work (v16 or newer)")
    missingprg = True
code, output = bash("which unar")
if code != 0:
    logger.error("You need The Unarchiver (unar) for this script to work (v1.10.1 or newer)")
    missingprg = True
else:
    code, output = bash("which lsar")
    if code != 0:
        logger.error("You need The Unarchiver (lsar) for this script to work (v1.10.1 or newer)")
        missingprg = True
code, output = bash("which lhasa")
if code != 0:
    logger.error("You need lhasa for this script to work (v0.3.1 or newer)")
    missingprg = True
if missingprg: exit(1)


Date = time.strftime("%Y-%m-%d")
if not Debug: logger.info("\n\nStarting PyRetroplay %s ...\n" % Date)
else: logger.info("\n\nStarting PyRetroplay Debug %s ...\n" % Date)

# see what directories should be checked
Dirs = []
if ProcessAGAGames or ProcessECSGames: Dirs.append(RetroGamesDir)
if ProcessDemos: Dirs.append(RetroDemosDir)
if ProcessJSTGames: Dirs.extend([RetroJOTDDir, RetroJSTDir])
if ProcessBetaGames: Dirs.append(RetroBetaDir)
if ProcessMagazines: Dirs.append(RetroMagDir)

# load current archive/directory database
if os.path.isfile(ArchDataFile):
    f=open(ArchDataFile,'r')
    Archives = json.load(f)
    f.close()

pbar = None
localfile = None
localsize = 0
def ftpwrite(data):
    global pbar
    global localfile
    global localsize
    localfile.write(data)
    localsize += len(data)
    try:
        pbar.update(localsize)
    except:
        pass

# check new files on Retroplay FTP
FTPList = []
FTPListLocal = []
if CheckFTP:
    logger.info("\nConnecting to ftp://grandis.nu ...")
    try:
        ftp = FTP("grandis.nu","ftp","amiga")
    except Exception, e:
        logger.error("FTP error: %s" % str(e))
        exit(1)
    checkFTPFiles(Dirs, FTPList, ftp)

    #FTPListLocal = [os.path.join(Local,x.decode('cp1252').replace("/",os.sep).encode('utf-8')) for x in FTPList]

    FTPListLocal = []
    for x in FTPList:
        try:
            y = x.decode('cp1252')
        except:
            print("\nERROR: cp1252 decode problem for",x)
        try:
            y = y.replace("/",os.sep)
        except:
            print("\nERROR: replace problem problem for",x)
        try:
            y = os.path.join(Local,y)
        except:
            print("\nERROR: path join problem problem for",x)
        FTPListLocal.append(y.encode('utf-8'))

    DownloadList = []
    for archive,localpath in zip(FTPList,FTPListLocal):
        if (not os.path.isfile(localpath)) and (not localpath in Archives['byarc']):
            DownloadList.append([archive,localpath])
    if DownloadList != [] and UpdateFromFTP:
        ftp.cwd("/")
        print("\n")
        ListSize = len(DownloadList)
        for idx, (archive, localpath) in enumerate(DownloadList):
            #logger.info("Downloading "+str(idx+1)+" of "+str(ListSize)+":"+archive.decode('cp1252').encode("utf-8"))
            logger.info(u'Downloading '+str(idx+1)+' of '+str(ListSize)+":"+archive)
            localfile = open(localpath, 'wb')
            ftppath = RetroplayPath + '/' + archive
            maxsize = ftp.size(ftppath)
            localsize = 0
            pbar = progressbar.ProgressBar(maxval = maxsize)
            pbar.start()
            if not Debug:
                ftp.retrbinary('RETR ' + ftppath, ftpwrite)
            localfile.close()
            pbar.finish()
    ftp.quit()
    stdout.write("\n")
    Archives['ftplist'] = FTPListLocal
    Archives['ftpdate'] = Date
else:
    if 'ftplist' in Archives:
        logger.info("\nFTP check disabled, using FTP list from %s" % Archives['ftpdate'])
        FTPListLocal = Archives['ftplist']

# collect information about existing archives
logger.info("\nScanning local archives and directories ...")
for Dir in Dirs:
    if ("JST" in Dir) or ("HD_Loaders" in Dir) or ("Beta" in Dir) or ("Magazines" in Dir):
        logger.error("Not yet implemented for: %s" % Dir)
        #TODO: implement handling of those...
    else:
        if "Demos" in Dir:
            checkLocalArchives(os.path.join(Local,Dir), "demos", Archives)
        else:
            checkLocalArchives(os.path.join(Local,Dir), "games", Archives)
stdout.write("\n")

# unpack archives that have newer installer version or are a new installers
# and generate AmigaDOS script for updating on real Amiga
AmigaScrNum = 1
AmigaScrName = "update%s-%d" % (Date,AmigaScrNum)
while os.path.isfile(AmigaScrName):
    AmigaScrNum += 1
    AmigaScrName = "update%s-%d" % (Date,AmigaScrNum)
Date = "%s-%d" % (Date,AmigaScrNum)
AmigaScrList1 = ""
AmigaScrList2 = ""
NewByArc = {}
Removed = []
#print("  ")
for unpackpath, arcdata in sorted(Archives['bydir'].iteritems()):
    dounpack = True
    try:
        arcpath = arcdata[0]['archivefull']
    except:
        logger.error(u'Something went wrong with the archive:\n%s' % arcpath)
        logger.debug(unpackpath)
        logger.debug(str(arcdata))
        #exit(1)
        continue
    if os.path.isdir(unpackpath):
        if len(arcdata) > 1:
            # check how many archives exist on Retroplay FTP
            OnFtpNum = 0
            OnFtpIdx = -1
            for idx, arc in enumerate(arcdata):
                #print(arc['archivefull'].encode("utf-8"))
                #if arc['archivefull'].encode("utf-8") in FTPListLocal:
                if arc['archivefull'].encode('utf-8') in FTPListLocal:
                    OnFtpNum += 1
                    OnFtpIdx = idx
            if OnFtpNum == 1:
                ArcIdx = OnFtpIdx
                newlzx = False
                maxver = arcdata[ArcIdx]['version']
            else: # if multiple archives exist, find the one with highest version
                versions = [x['version'] for x in arcdata]
                maxver = max(versions)
                minver = min(versions)
                ArcIdx = versions.index(maxver)
                newlzx = False
                if maxver == minver:
                    #check if new archive is lzx
                    for idx,arc in enumerate(arcdata):
                        if arc['archivefull'][-4:].lower() == ".lzx":
                            ArcIdx = idx
                            newlzx = True
            if newlzx and (ArcIdx == 0):    # quickfix for two EmeraldMinesCD archives
                dounpack = False
            else:
                logger.info("Found newer version (%s) for: %s " % (maxver,unpackpath))
            for idx, data in enumerate(arcdata):
                if (idx != ArcIdx):
                    try:
                        del Archives['byarc'][data['archivefull']]
                    except:
                        pass
                    #if os.path.isfile(data['archivefull']) and not data['archivefull'].encode('utf-8') in FTPListLocal:
                    if os.path.isfile(data['archivefull']) and not data['archivefull'].encode('utf-8') in FTPListLocal:
                        logger.info(u'Deleting old archive: %s' % data['archivefull'])
                        if not Debug:
                            try:
                                os.remove(data['archivefull'])
                            except:
                                logger.info(u'File already deleted: %s' % data['archivefull'])
            if dounpack:
                logger.info("Deleting directory: %s" % unpackpath)
                if not Debug:
                    try:
                        shutil.rmtree(unpackpath)
                    except:
                        logger.debug("Directory already deleted: %s" % unpackpath)
                    try:
                        os.remove(unpackpath + ".info")
                    except:
                        logger.debug("File already deleted: %s.info" % unpackpath)
            arcpath = arcdata[ArcIdx]['archivefull']
            Archives['bydir'][unpackpath] = [arcdata[ArcIdx]]
        else:
            dounpack = False
        NewByArc[arcpath] = unpackpath
    if dounpack:
        if os.path.isfile(arcpath):
            logger.info("Unpacking new archive to: %s" % unpackpath)
            unpackdir = os.path.dirname(unpackpath)
            if not Debug:
                # try first with 7z
                code, output = bash('7z -y -ssc -sccutf-8 -o"%s" x "%s"' % (unpackdir, arcpath) )
                if code != 0:
                    logger.debug("7zip finished with code %d" % code)
                    # if that fails, try with unar
                    logger.debug("Deleting game/demo: %s" % unpackpath)
                    try:
                        shutil.rmtree(unpackpath)
                    except:
                        pass
                    try:
                        os.remove(unpackpath + ".info")
                    except:
                        pass
                    logger.debug(u"Trying the same archive with unarchiver...")
                    code, output = bash('unar -o "%s" -D "%s"' % (unpackdir, arcpath) )
                    if code != 0:
                        logger.debug("unar finished with code %d" % code)
                        # if that fails, try with lhasa
                        logger.debug("Deleting game/demo: %s" % unpackpath)
                        try:
                            shutil.rmtree(unpackpath)
                        except:
                            pass
                        try:
                            os.remove(unpackpath + ".info")
                        except:
                            pass
                        logger.debug(u"Trying the same archive with lhasa...")
                        code, output = bash('lhasa -xw="%s" "%s"' % (unpackdir, arcpath) )
            else: code = 0
            if code == 0:
                NewByArc[arcpath] = unpackpath
                if GenerateAmigaScript:
                    # copy archive to AmigaDOS update directory
                    updarc = arcpath.replace(Local + os.sep, "")
                    updarc = os.path.join(AmigaScrName + "-files", updarc)
                    updarc = updarc.replace("Commodore_Amiga_-_","")
                    updpath = os.path.dirname(updarc)
                    if not os.path.isdir(updpath):
                        os.makedirs(updpath)
                    shutil.copy(arcpath,updarc)
                    # AmigaDOS script generation
                    s = AmigaDosUpdateList.replace("##path##", unpackpath)
                    s = s.replace("/#/","/'#/")
                    AmigaScrList1 += s
                    if updarc.lower().find(".lha") >= 0:
                        s = AmigaDosUpdateLha.replace("##path##", unpackpath)
                    else:
                        s = AmigaDosUpdateLzx.replace("##path##", unpackpath)
                    s = s.replace("##paths##", unpackdir)
                    s = s.replace("##arc##", updarc)
                    s = s.replace("/#/","/'#/")
                    AmigaScrList2 += s
            else:
                logger.error(u'Something went wrong while unpacking:\n%s' % output)
        else:
            Removed.append(unpackpath)
for unpackpath in Removed:
    del Archives['bydir'][unpackpath]
Archives['byarc'] = NewByArc

# check for archives removed from Retroplay FTP
AmigaScrList3 = ""
AmigaScrList4 = ""
if FTPListLocal != []:
    RemovedFTP = []
    for archive in sorted(Archives['byarc']):
        #if not archive.encode('utf-8') in FTPListLocal: RemovedFTP.append(archive)
        if not archive.encode('utf-8') in FTPListLocal: RemovedFTP.append(archive)
    if RemovedFTP != []:
        logger.info("\nThese archives no longer exist on Retroplay FTP:")
        for archive in sorted(RemovedFTP):
            path = Archives['byarc'][archive]
            unpacked = ""
            if os.path.isdir(path): unpacked = "\n    Unpacked at: " + path
            #logger.info("  %s %s" % (archive.encode('utf-8'),unpacked) )
            try:
                logger.info(u'  %s %s' % (archive,unpacked) )
            except Exception, e:
                print(e)
                print(unpacked)
                print(archive)
                exit(1)
            if DeleteNonFTPGames:
                logger.info("   Deleting archive and game installation...")
                if not Debug:
                    shutil.rmtree(path)
                    try:
                        os.remove(path + ".info")
                    except:
                        logger.info("File already deleted: %s.info" % path)
                    try:
                        os.remove(archive)
                    except:
                        #logger.info("File already deleted: %s" % archive.encode('utf-8'))
                        logger.info("File already deleted: %s" % archive)
                s = AmigaDosDeleteList.replace("##path##", path)
                s = s.replace("/#/","/'#/")
                AmigaScrList3 += s
                s = AmigaDosDeleteCmd.replace("##path##", path)
                s = s.replace("/#/","/'#/")
                AmigaScrList4 += s
        if not DeleteNonFTPGames:
            logger.info("If they are not needed, they should be removed manually.")

if (AmigaScrList1 != "") or (AmigaScrList4 != ""):
    AmigaScript = codecs.open(AmigaScrName, "w", "cp1252")  #TODO: check on real Amiga if this is correct
    s = AmigaDosStart.replace("##date##", Date)
    s = s.replace("##games##", Games)
    s = s.replace("##aga##", AGA)
    s = s.replace("##demos##", Demos)
    AmigaScript.write(s)
    if AmigaScrList1 != "":
        AmigaScript.write(AmigaDosUpdateStart)
        AmigaScript.write(AmigaScrList1)
        AmigaScript.write(AmigaDosContinue)
        AmigaScript.write(AmigaScrList2)
    if AmigaScrList4 != "":
        AmigaScript.write(AmigaDosDeleteStart)
        AmigaScript.write(AmigaScrList3)
        AmigaScript.write(AmigaDosContinue)
        AmigaScript.write(AmigaScrList4)
    s = AmigaDosEnd.replace("##date##", Date)
    AmigaScript.write(s)
    logger.info("\nAmigaDOS update script genrated as: %s" % AmigaScrName)
    logger.info("Copy the script and its files to real Amiga.")

# make backup of old database
if os.path.isfile(ArchDataFile) and not Debug:
    backup = ArchDataFile + ".bak"
    if os.path.isfile(backup):
        os.remove(backup)
    shutil.move(ArchDataFile, backup)
# save current database
logger.info("\nSaving current database...")
if not Debug: datafile = open(ArchDataFile, "w")
else: datafile = open(ArchDataFile + ".dbg", "w")
json.dump(Archives, datafile, sort_keys=True, indent=4, separators=(',', ': '), encoding="utf-8")
datafile.close()
if not Debug: os.chmod(ArchDataFile,0o664)

logger.info("\nDone.")
print("Check PyRetroplay.log for details")

